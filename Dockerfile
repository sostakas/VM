FROM golang
RUN mkdir /app
ADD src /app
WORKDIR /app
RUN go build main.go
CMD [ "./main" ]